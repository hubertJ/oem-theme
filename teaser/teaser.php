<?php
/*
Template Name: Teaser
*/

$themedir = get_template_directory_uri();
?>

<!DOCTYPE html>
<html lang="de-DE">
<head>
	<title>&Ouml;M 2018</title>
	<meta property="og:title" content="Österreichische Meisterschaft 2018" />
	<meta property="og:image" content="<?php echo $themedir; ?>/teaser/linkvorschau.jpg" />
	<meta property="og:url" content="http://oem.indes.at" />
	<meta property="og:type" content="website" />
	<meta property="og:description" content=" im langen Schwert, Rapier und Säbel. Vom 26. bis zum 27. Mai findet wieder die ÖM statt, dieses Mal in der Unionhalle in Graz." />
	<link rel="stylesheet" href="<?php echo $themedir; ?>/teaser/teaserstyle.css">
	<!-- Piwik -->
	<script type="text/javascript">
	  var _paq = _paq || [];
	  /* tracker methods like "setCustomDimension" should be called before "trackPageView" */
	  _paq.push(['trackPageView']);
	  _paq.push(['enableLinkTracking']);
	  (function() {
	    var u="//analytics.indes.at/";
	    _paq.push(['setTrackerUrl', u+'piwik.php']);
	    _paq.push(['setSiteId', '7']);
	    var d=document, g=d.createElement('script'), s=d.getElementsByTagName('script')[0];
	    g.type='text/javascript'; g.async=true; g.defer=true; g.src=u+'piwik.js'; s.parentNode.insertBefore(g,s);
	  })();
	</script>
	<!-- End Piwik Code -->
</head>


<body>

<video playsinline autoplay muted loop poster="<?php echo $themedir; ?>/teaser/background.jpg" class="fullscreen">
    <source src="<?php echo $themedir; ?>/teaser/video.mp4" type="video/mp4">
</video>


<div class="content">
	<div class="text">
		<a href="http://www.historisches-fechten.at"><img src="<?php echo $themedir; ?>/teaser/StartseiteLogo-OEFHF.png"></a><br /><br />
		<span class="title"><span class="red">&Ouml;</span>sterreichische <span class="red">M</span>eisterschaft <span class="red">2018</span></span><br />
		im Langen Schwert, Rapier und S&auml;bel.<br /><br />
		26.-27. Mai 2018<br />
		Unionhalle, Graz<br />
		<br /><br />
		Anmeldung und weitere Details ab dem 1. J&auml;nner 2018.
		<div class="logos">
			<a href="http://www.indes.at"><img src="<?php echo $themedir; ?>/teaser/INDESLogo_k.png"></a>&nbsp;&nbsp;&nbsp;<a href="https://www.sportunion-steiermark.at/"><img src="<?php echo $themedir; ?>/teaser/SportunionLogo.png"></a>
		</div>
	</div>
	<div class="old_link">
		<a href="http://oem.indes.at/?page_id=6">Seite der &Ouml;M 2017</a>
	</div>
</div>

</body>
</html>