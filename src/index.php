<?php
	get_header();
	global $lang, $oempages;
?>

<header id="desktop">
	<nav>
		<input type="checkbox" id="show-menu" role="button">
		<label for="show-menu" class="show-menu">
				<div></div>
				<div></div>
				<div></div>
		</label>

		<?php

			echo '<ul>';
			$i = 1;
			foreach ( $menu as $m ) {
				$j = $i++;
				echo '<li><a href="?page_id='. $oempages[$j]['object_id'] .'">'. $m['title'] .'</a></li>';
			}
			echo '</ul>';

		?>
	</nav>
</header>

<div class="single">
	<div class="single_content">
		<?php
			if ( have_posts() ) : while ( have_posts() ) : the_post();
				the_content();
				$background = get_post_meta($post->ID, 'background', true);
			endwhile;
			else :
				_e( 'Sorry, no posts matched your criteria.', 'textdomain' );
			endif;
		?>
	</div>
	<div class="single_image" style="background: url(<?php	echo $background;	?>);">

	</div>
</div>

<?php
	get_footer(); 
?>