function slideHeights() {
	var i = 0;
	jQuery( ".parallax__group" ).each( function()  {
		i = i+1;
		var childheight = jQuery(this).find(".layer--content").height();
		if ( childheight > "0" ) {
			if (i == 2 && jQuery(window).width() < 1400) {
				jQuery(this).css({ minHeight: childheight+250 });  
			} else {
				jQuery(this).css({ minHeight: childheight+150 });  
			}
		}
	});
}

// Workaround for new behaviour from Chrome (Bug?)
var x = 999;
jQuery(".parallax").scroll( function() {
	x = x+1;
	console.log(x);
	jQuery( "#desktop" ).css("z-index", x )
});



jQuery(window).bind("load", function() {
	slideHeights()
});
jQuery( "input" ).click(function() {
	slideHeights()
});
jQuery(window).bind("load", function() {
	setTimeout(function() {
		slideHeights()
	}, 1000);
});
jQuery(window).on("orientationchange", function( event ) {
	setTimeout(function() {
		slideHeights()
	}, 250);
});
jQuery(window).resize( function( event ) {
	setTimeout(function() {
		slideHeights()
	}, 250);
});
jQuery( "nav a").click(function() {
	jQuery( "#show-menu" ).prop( "checked", false );
});