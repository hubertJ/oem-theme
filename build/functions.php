<?php

remove_filter('the_content', 'wpautop');

/*
function oemtheme_enqueue_styles() {

	$parent_style = 'twentyseventeen-style'; // This is 'twentyfifteen-style' for the Twenty Fifteen theme.

	wp_enqueue_style( $parent_style, get_template_directory_uri() . '/style.css' );
	wp_enqueue_style( 'child-style',
		get_stylesheet_directory_uri() . '/style.css',
		array( $parent_style ),
		wp_get_theme()->get('Version')
	);
}
add_action( 'wp_enqueue_scripts', 'oemtheme_enqueue_styles' );
*/

if ( is_plugin_active('polylang/polylang.php') ) {
	$lang = pll_current_language();	
}

if ( ! is_admin() ) {
	wp_enqueue_script("jquery");
	wp_enqueue_style( 'style', get_stylesheet_uri() );
}


function oem_scripts() {
	if ( ! is_admin() ) {
		wp_register_script('oem-scripts', get_stylesheet_directory_uri().'/js/scripts.js', array( 'jquery' ), '0', true);
		wp_enqueue_script('oem-scripts');
	}
}
add_action( 'wp_enqueue_scripts', 'oem_scripts', 9999);


if ( ! is_admin() ) {
	function wp_get_menu_array($current_menu) {
		$array_menu = wp_get_nav_menu_items($current_menu);
		$menu = array();
		$i = 1;
		foreach ($array_menu as $m) {
			if (empty($m->menu_item_parent)) {
				$j = $i++;
				$menu[$j] = array();
				$menu[$j]['ID']		= $m->ID;
				$menu[$j]['object_id'] = $m->object_id;
				$menu[$j]['title']	= $m->title;
			}
		}
		return $menu;
	} 

	if ( is_plugin_active('polylang/polylang.php') ) {
		$lang = pll_current_language();
		$m = 'top_'.$lang;	
	} else {
		$m = 'top_menu';
	}

	$menu = wp_get_menu_array($m);
	$oempages = array();
	$i = 1;
	foreach ($menu as $o) {
		$j = $i++;
		$oempages[$j] = array();
		$oempages[$j]['order']	= $j;
		$oempages[$j]['ID']		= $o['ID'];
		$oempages[$j]['object_id']		= $o['object_id'];
		$oempages[$j]['title']	= str_replace(' ', '_', $o['title']);
	}
}


function theme_setup() {
	// Add default posts and comments RSS feed links to head.
	add_theme_support( 'automatic-feed-links' );

	/*
	 * Let WordPress manage the document title.
	 * By adding theme support, we declare that this theme does not use a
	 * hard-coded <title> tag in the document head, and expect WordPress to
	 * provide it for us.
	 */
	add_theme_support( 'title-tag' );

	/*
	 * Enable support for Post Thumbnails on posts and pages.
	 *
	 * @link https://developer.wordpress.org/themes/functionality/featured-images-post-thumbnails/
	 */
	add_theme_support( 'post-thumbnails' );

	add_image_size( 'twentyseventeen-featured-image', 2000, 1200, true );

	add_image_size( 'twentyseventeen-thumbnail-avatar', 100, 100, true );

		/*
	 * Switch default core markup for search form, comment form, and comments
	 * to output valid HTML5.
	 */
	add_theme_support( 'html5', array(
		'comment-form',
		'comment-list',
		'gallery',
		'caption',
	) );

	/*
	 * Enable support for Post Formats.
	 *
	 * See: https://codex.wordpress.org/Post_Formats
	 */
	add_theme_support( 'post-formats', array(
		'aside',
		'image',
		'video',
		'quote',
		'link',
		'gallery',
		'audio',
	) );

	/* Disable default padding styles for the Toolbar */
	add_theme_support( 'admin-bar', array( 'callback' => '__return_false' ) );

	
}
add_action( 'after_setup_theme', 'theme_setup' );


add_filter( 'show_admin_bar', '__return_false' );

?>