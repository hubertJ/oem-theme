<?php ?>

<!DOCTYPE html>
<html <?php language_attributes(); ?> >
<head>
	<meta charset="<?php bloginfo( 'charset' ); ?>">
	<meta name="viewport" content="width=device-width, initial-scale=1.0, user-scalable=yes">
	<link rel="profile" href="http://gmpg.org/xfn/11">
	<?php wp_head(); ?>
	<link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/font-awesome/4.7.0/css/font-awesome.min.css">
	<noscript>
		<link rel="stylesheet" type="text/css" href="<?php echo get_stylesheet_directory_uri(); ?>/nojs.css" />
	</noscript>
</head>

<body>

<?php
	global $menu, $oempages;
?>


<header id="mobile">
<!--
	<div id="nav_hamburger">
	  <a href="#">
		 <div></div>
			<div></div>
			<div></div>
		 </a>
	</div>
-->
	<nav>
	  <input type="checkbox" id="show-menu" role="button">
	  <label for="show-menu" class="show-menu">
			<div></div>
			<div></div>
			<div></div>
	  </label>

	  <?php

		 echo '<ul>';
		 $i = 1;
		 foreach ( $menu as $m ) {
			$j = $i++;
			echo '<li><a href="#'. $oempages[$j]['title'] .'">'. $m['title'] .'</a></li>';
		 }
		 echo '</ul>';

	  ?>
	</nav>
</header>