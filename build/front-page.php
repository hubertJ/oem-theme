<?php

get_header();

global $lang, $oempages;

?>

<div class="parallax">

	<?php

		foreach ($oempages as $page) {
			if ($page['order'] == 1 ) {
				// $post = get_post($page['object_id']);
				$postid = pll_get_post($page['object_id'], $lang);
				$post = get_post($postid);
				$content = apply_filters('the_content', $post->post_content);
	?>

					<div id="<?php echo $page['title']; ?>" class="parallax__group">
						<div class="parallax__layer parallax__layer--base">
							<div class="lead">
								<div class="text">

									<?php 
										echo $content;  
									?>  
									<ul><?php pll_the_languages(array('show_flags'=>'1','show_names'=>'0','force_home'=>'1'));?></ul>                  

								</div>
								<div class="next_slide">
									<a href="#<?php echo $oempages[2]['title']; ?>"><i class="fa fa-chevron-down" aria-hidden="true"></i></a>
								</div>
							</div>
						</div>
						<div class="parallax__layer parallax__layer--back">
							<video playsinline autoplay muted loop poster="<?php echo get_stylesheet_directory_uri(); ?>/assets/background.jpg" class="fullscreen">
								<source src="<?php echo get_stylesheet_directory_uri(); ?>/assets/video.mp4" type="video/mp4">
							</video>
						</div>
					</div>

					<header id="desktop" style="z-index:999">
						<nav>
							<input type="checkbox" id="show-menu" role="button">
							<label for="show-menu" class="show-menu">
									<div></div>
									<div></div>
									<div></div>
							</label>

							<?php

								echo '<ul>';
								$i = 1;
								foreach ( $menu as $m ) {
									$j = $i++;
									echo '<li><a href="#'. $oempages[$j]['title'] .'">'. $m['title'] .'</a></li>';
								}
								echo '</ul>';

							?>
						</nav>
					</header>

	<?php
			}
			else {
					$postid = pll_get_post($page['object_id'], $lang);
					$post = get_post($postid);
					$content = apply_filters('the_content', $post->post_content);

					$overlay1 = get_post_meta($post->ID, 'overlay1', true);
					$overlay2 = get_post_meta($post->ID, 'overlay2', true);
					$zindex = get_post_meta($post->ID, 'zindex', true);
					$margintop = get_post_meta($post->ID, 'margintop', true);
					$background = get_post_meta($post->ID, 'background', true);
	?>        

			 <div id="<?php echo $page['title']; ?>" class="parallax__group">
				<div class="parallax__layer parallax__layer--fore">
					<div class="img_container">
					<?php 
						if ( $overlay1 ):
							echo $overlay1;
						endif;
						if ( $overlay2 ):
							echo "<br />";
							echo $overlay2;
						endif; 
					?>
					</div>
				</div>
				<div class="parallax__layer parallax__layer--base">
					<div class="layer--content <?php if ( $overlay1 ): echo "overlay"; endif; ?>">
					 
						<?php 
							echo $content; 
						?>

					</div>
				</div>
			</div>

			<div class="parallax__group" <?php if($zindex) { echo 'style="z-index:'.$zindex.'"'; } ?> >
				<div class="parallax__layer parallax__layer--base">
					<div class="layer--content">


					</div>
				</div>
				<div class="parallax__layer parallax__layer--back" style="background: #000 url(<?php echo $background; ?>);<?php if($margintop) { echo 'margin-top:'.$margintop; } ?>" ></div>

			</div>

	<?php
				}
			}
	?>

<footer>  
	<?php
		$posttitle = "footer_".$lang;
		$post = get_page_by_title($posttitle);
		$content = apply_filters('the_content', $post->post_content);
		echo $content;
	?>
</footer> 

</div>



<?php get_footer(); ?>