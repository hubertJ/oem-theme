var gulp          = require('gulp'),
    browserSync   = require('browser-sync').create(),
    fileinclude   = require('gulp-file-include'),
    log           = require('fancy-log'),
    plugins       = require("gulp-load-plugins")({
                        pattern: ['gulp-*', 'gulp.*', 'main-bower-files'],
                        replaceString: /\bgulp[\-.]/
                    });    
 
gulp.task('browser-sync', function() {
    browserSync.init({
        // lokale Wordpress Installation
        proxy: "localhost/wordpress"
    });
});

gulp.task('js', function() {
    var jsFiles = ['src/js/*'];
    // var array = plugins.mainBowerFiles();
    // var index = array.indexOf('/home/hubert/dev/oem-theme/bower_components/infinite-scroll/js/index.js');
    // log(index);

    // return gulp.src(plugins.mainBowerFiles().concat(jsFiles)
    return gulp.src(jsFiles)
        .pipe(plugins.filter('**/*.js'))
        .pipe(plugins.concat('scripts.js'))
        .pipe(plugins.uglify())
        .pipe(gulp.dest('build/js'));
        
});

gulp.task('php', function () {
    return gulp.src('src/**/*.php')
        .pipe(gulp.dest('build'));
});

gulp.task('styles', function() {
    gulp.src('src/scss/*.scss')
        .pipe(plugins.sass({outputStyle: 'expanded'}).on('error', plugins.sass.logError))
        .pipe(plugins.autoprefixer({
            browsers: ['>0.21% in at'],
            cascade: true
        }))
        .pipe(plugins.cleanCss({compatibility: '*'}))
        .pipe(gulp.dest('build'));
    return gulp.src('src/**/*.css')
        .pipe(gulp.dest('build'));
});

gulp.task('fileinclude', function() {
    gulp.src(['./src/anleitung/*.html'])
        .pipe(fileinclude({
          prefix: '@@',
          basepath: '@file'
        }))
        .pipe(gulp.dest('./build/anleitung/'));
});

gulp.task('dist', function() {
    gulp.src('build/**/*')
        .pipe(gulp.dest('dist'))
})

gulp.task('watch', ['styles', 'js', 'php', 'fileinclude'], function(){
    gulp.watch('./src/**/*.php', ['php']).on('change', browserSync.reload); 
    gulp.watch('./src/**/*.scss', ['styles']).on('change', browserSync.reload);
    gulp.watch('./src/**/*.css', ['styles']).on('change', browserSync.reload); 
    gulp.watch('./src/js/*.js', ['js']).on('change', browserSync.reload); 
})

gulp.task('default', ['browser-sync', 'watch']);